/*
 * Copyright © 2009 Christopher Eby <kreed@kreed.org>
 *
 * This file is part of Ett.
 *
 * Ett is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * Ett is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See <http://www.gnu.org/licenses/> for the full license text.
 */

#include "util.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void
result(const char *name, bool pass)
{
	printf("%s test %s\n", name, pass ? "passed" : "failed");
}

int
main(void)
{
	bool pass;

	char test1[] = "\t foo\r\r\r";
	pass = strcmp(chomp(test1), "foo") == 0;
	result("chomp()", pass);

	char test2[] = "=?utf-8?B?Zm9v?=";
	pass = strcmp(decode(test2), "foo") == 0;
	result("decode() - base64", pass);

	char test3[] = "=?iso-8859-1?Q?foo_bar?=";
	pass = strcmp(decode(test3), "foo bar") == 0;
	result("decode() - escape _", pass);

	char test4[] = "=?iso-8859-1?Q?foo%20bar?=";
	pass = strcmp(decode(test4), "foo bar") == 0;
	result("decode() - escape %", pass);

	return EXIT_SUCCESS;
}

