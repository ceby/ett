DEST=/usr/local/bin

LDFLAGS+=-lpthread

ett: main.o util.o
	$(CC) $(CFLAGS) $(LDFLAGS) main.o util.o -o $@

test: test.o util.o
	$(CC) $(CFLAGS) $(LDFLAGS) test.o util.o -o $@

util.o: util.h
main.o: util.h
test.o: util.h

.PHONY: install

install: ett
	install -sm 4755 $< $(DEST)
