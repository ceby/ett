/*
 * Copyright © 2008-2009 Christopher Eby <kreed@kreed.org>
 *
 * This file is part of Ett.
 *
 * Ett is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * Ett is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See <http://www.gnu.org/licenses/> for the full license text.
 */

#include "util.h"
#include <errno.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <pthread.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static const char *prog;

static const int buffer_size = 512;
static const int worker_count = 2;

typedef struct _node node;
static struct _node {
	int id;
	node *next;
} *connection_queue = NULL;
static pthread_mutex_t connection_queue_mutex;
static pthread_cond_t connection_queue_cond;

#define push_msg(sock, msg) \
	send(sock, msg "\r\n", sizeof(msg) + 1, 0)

#define cut_line(str, offset) \
	if (str) { \
		str += offset; \
		char *nul; \
		if ((nul = index(str, '\n'))) \
			*nul = '\0'; \
		setenv(#str, decode(chomp(str)), true); \
	}

static void
process_connection(int csock)
{
	struct timeval tv = { 15, 0 };
	setsockopt(csock, SOL_SOCKET, SO_RCVTIMEO, (char*)&tv, sizeof(tv));

	char line[buffer_size];
	ssize_t line_len;

	char *data;
	size_t data_capacity = 4096;
	size_t data_len;
	bool reading_data = false;

	data = (char*)malloc(sizeof(char) * data_capacity);

	push_msg(csock, "220 You may speak.");

	while ((line_len = recv(csock, line, buffer_size - 1, 0)) > 0) {
		line[line_len] = '\0';

		if (reading_data) {
			size_t new_len = data_len + line_len;
			if (data_capacity < new_len) {
				data_capacity = new_len / 2 * 3 + 1;
				data = (char*)realloc(data, data_capacity);
			}

			memcpy(data + data_len, line, line_len);
			data_len = new_len;
			data[data_len] = '\0';

			if (!strcmp("\r\n.\r\n", data + data_len - 5)) {
				if (!fork()) {
					char *receiver = strstr(data, "\nTo:");
					char *sender = strstr(data, "\nFrom:");
					char *subject = strstr(data, "\nSubject:");

					cut_line(receiver, 4);
					cut_line(sender, 6);
					cut_line(subject, 9);

					execlp(prog, prog, NULL);
					exit(EXIT_SUCCESS);
				}

				reading_data = false;

				push_msg(csock, "250 OK");
			}
		} else if (!strncmp(line, "EHLO", 4) || !strncmp(line, "HELO", 4)) {
			push_msg(csock, "250 OK");
		} else if (!strncmp(line, "MAIL", 4)) {
			push_msg(csock, "250 OK");
		} else if (!strncmp(line, "RCPT", 4)) {
			push_msg(csock, "250 OK");
		} else if (!strncmp(line, "DATA", 4)) {
			reading_data = true;
			data_len = 0;
			push_msg(csock, "354 Start mail input; end with [CRLF].[CRLF]");
		} else if (!strncmp(line, "QUIT", 4)) {
			push_msg(csock, "221 Bye");
			break;
		}
	}

	free(data);
	close(csock);
}

static void*
worker_main(void *data)
{
	while (pthread_cond_wait(&connection_queue_cond, &connection_queue_mutex) == 0) {
		while (connection_queue) {
			node *oldest_node = connection_queue;
			connection_queue = oldest_node->next;

			pthread_mutex_unlock(&connection_queue_mutex);
			process_connection(oldest_node->id);
			free(oldest_node);
			pthread_mutex_lock(&connection_queue_mutex);
		}
	}

	return NULL;
}

static void
queue_connection(int id)
{
	node *new_node = malloc(sizeof(node));
	new_node->id = id;

	pthread_mutex_lock(&connection_queue_mutex);

	new_node->next = connection_queue;
	connection_queue = new_node;

	pthread_mutex_unlock(&connection_queue_mutex);
	pthread_cond_signal(&connection_queue_cond);
}

int
main(int argc, char **argv)
{
	int sock;
	struct sockaddr_in cadr;
	socklen_t cadr_len = sizeof(cadr);

	{
		bool do_fork = true;
		int port = 25;

		static char usage[] =
			"Usage: ett /path/to/script\n\n"
			"Options:\n"
			"	-f	Run in foreground; do not fork\n"
			"	-p	Listen on port ARG\n";
		for (;;)
			switch (getopt(argc, argv, "fp:h?")) {
				case 'f':
					do_fork = false;
					break;
				case 'p':
					port = atoi(optarg);
					break;
				case 'h': case '?':
					fputs(usage, stdout);
					return EXIT_SUCCESS;
				case -1:
					if (optind < argc)
						goto escape;
					/* else fall through */
				default:
					fputs(usage, stderr);
					return EXIT_FAILURE;
			}
	escape:
		prog = argv[optind];

		/* automatically kill zombie children */
		struct sigaction sig_act;
		sig_act.sa_handler = SIG_IGN;
		sig_act.sa_flags = 0;
		sigemptyset(&sig_act.sa_mask);
		sigaction(SIGCHLD, &sig_act, NULL);

		sock = socket(AF_INET, SOCK_STREAM, 6);
		if (sock == -1) {
			perror("Failed to open socket");
			return EXIT_FAILURE;
		}

		struct sockaddr_in adr;
		adr.sin_family = AF_INET;
		adr.sin_addr.s_addr = htonl(INADDR_ANY);
		adr.sin_port = htons(port);

		if (bind(sock, (struct sockaddr*)&adr, sizeof(adr)) == -1) {
			fprintf(stderr, "Failed to bind to port %d: %s\n", port, strerror(errno));
			return EXIT_FAILURE;
		}

		if (listen(sock, 5) == -1) {
			fprintf(stderr, "Failed to listen on port %d: %s\n", port, strerror(errno));
			return EXIT_FAILURE;
		}

		if (setgid(getgid()) == -1 || setuid(getuid()) == -1) {
			perror("Failed to drop privelages");
			return EXIT_FAILURE;
		}

		if (do_fork) {
			if (fork())
				return EXIT_SUCCESS;

			int fd = open("/dev/null", O_RDWR);
			dup2(fd, STDIN_FILENO);
			dup2(fd, STDOUT_FILENO);
			dup2(fd, STDERR_FILENO);
		}

		/* create workers */
		pthread_t thread;
		int i;
		for (i = worker_count; --i != -1; )
			pthread_create(&thread, NULL, worker_main, NULL);
	}

	for (;;)
		queue_connection(accept(sock, (struct sockaddr *)&cadr, &cadr_len));

	close(sock);

	return EXIT_FAILURE;
}

