/*
 * Copyright © 2008-2009 Christopher Eby <kreed@kreed.org>
 *
 * This file is part of Ett.
 *
 * Ett is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * Ett is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See <http://www.gnu.org/licenses/> for the full license text.
 */

#include <stdbool.h>
#include <string.h>

static const unsigned char base64_map[256] = {
	253, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	255, 255, 255, 255, 255, 255, 255, 62,  255, 255, 255, 63,
	52,  53,  54,  55,  56,  57,  58,  59,  60,  61,  255, 255,
	255, 254, 255, 255, 255, 0,   1,   2,   3,   4,   5,   6,
	7,   8,   9,   10,  11,  12,  13,  14,  15,  16,  17,  18,
	19,  20,  21,  22,  23,  24,  25,  255, 255, 255, 255, 255,
	255, 26,  27,  28,  29,  30,  31,  32,  33,  34,  35,  36,
	37,  38,  39,  40,  41,  42,  43,  44,  45,  46,  47,  48,
	49,  50,  51,  255, 255, 255, 255, 255, 255, 255, 255, 255,
	255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	255, 255, 255, 255
};

static size_t
decode_base64(unsigned char *str)
{
	unsigned char *src = str;
	unsigned char *dest = str;
	unsigned long block = 0;
	size_t hex_count = 0;
	size_t oct_count = 3;

	for (;; ++src, block <<= 6) {
		unsigned long hex = base64_map[*src & 0xff];
		switch (hex) {
		case 253: /* nul == end of input */
			return dest - str;
		case 255: /* unknown */
			continue;
		case 254: /* '=' == padding */
			--oct_count;
			break;
		default: /* base64 digit */
			block |= hex;
			break;
		}

		if (++hex_count == 4) {
				*dest++ = (unsigned char)(255 & (block >> 16));
			if (oct_count > 1)
				*dest++ = (unsigned char)(255 & (block >> 8));
			if (oct_count == 3)
				*dest++ = (unsigned char)(255 & (block));
			hex_count = block = 0;
		}
	}
}

static char
hex_to_int(char c, bool *ok)
{
	if ('0' <= c && c <= '9')
		return c - '0';
	else if ('a' <= c && c <= 'f')
		return c - 'a' + 10;
	else if ('A' <= c && c <= 'F')
		return c - 'A' + 10;
	*ok = false;
	return 0;
}

static size_t
decode_escaped(char *str, size_t len)
{
	char *pos;

	for (pos = str; (pos = index(pos, '_')); ++pos)
		*pos = ' ';
	for (pos = str; (pos = index(pos, '%')); ++pos) {
		bool ok = true;
		char hi = hex_to_int(pos[1], &ok);
		char lo = hex_to_int(pos[2], &ok);

		if (!ok)
			continue;

		*pos = (hi << 4) + lo;
		pos[1] = pos[2] = '\0';
	}

	char *src;
	char *dest;
	src = dest = index(str, '\0');

	for (; src - str < len; ++src)
		if (*src)
			*dest++ = *src;

	return dest - str;
}

char*
decode(char *str)
{
	char *pos = str;
	char *block;

	while ((block = strstr(pos, "=?"))) {
		char *mode = index(block + 2, '?');
		char *end = strstr(block, "?=");
		if (!(mode && end)) {
			pos = block + 1;
			continue;
		}

		mode += 3;
		*end = '\0';
		size_t len;

		switch (mode[-2]) {
		case 'B': case 'b':
			len = decode_base64((unsigned char*)mode);
			break;
		case 'Q': case 'q':
			len = decode_escaped(mode, end - mode);
			break;
		default:
			continue;
		}

		strcpy(block, mode);
		strcpy(block + len, end + 2);
	}

	return str;
}

#define chompable(str) \
	*str == ' ' || *str == '\t' || *str == '\r'

char*
chomp(char *str)
{
	char *end = index(str, '\0') - 1;

	while (chompable(str))
		++str;
	while (chompable(end))
		--end;

	end[1] = '\0';
	return str;
}

